package model;

public class LocalBaseUrl {

    private String baseUrlAccValue = "http://192.168.1.82";
    private String baseUrlPowerValue = "http://192.168.1.84";
    private String basePathValue = "/set/m2m/outputs?type=rele&number=0&";
    private String offValue = "value=0&";
    private String onValue = "value=1&";
    private String hashAccValue = "hash=d033e22ae348aeb5660fc2140aec35850c4da997";
    private String hashPowerValue = "hash=d033e22ae348aeb5660fc2140aec35850c4da997";
    public String sendAccOn = baseUrlAccValue + basePathValue + onValue + hashAccValue;
    public String sendAccOff = baseUrlAccValue + basePathValue + offValue + hashAccValue;
    public String sendPowerOn = baseUrlPowerValue + basePathValue + onValue + hashPowerValue;
    public String sendPowerOff = baseUrlPowerValue + basePathValue + offValue + hashPowerValue;
}
