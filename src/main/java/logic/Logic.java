package logic;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import model.LocalBaseUrl;

public class Logic {

    /**
     * Метод включает / отключает реле ACC
     *
     * @param numberOfAttempts - кол-во включений / отключений ACC
     * @param sleepTime        - время ожидания между включениями / отключениями ACC
     * @throws InterruptedException
     */
    public void customAccOnOffWithSleep(int numberOfAttempts, int sleepTime, LocalBaseUrl localBaseUrl) throws InterruptedException {
        for (int i = 0; i < numberOfAttempts; i++) {
            RestAssured.get(localBaseUrl.sendAccOn);
            System.out.println("Сработала команда включения зажигания. Шаг: " + (i + 1));

            waitingBetweenAttempts(sleepTime);

            RestAssured.get(localBaseUrl.sendAccOff);
            System.out.println("Сработала команда выключения зажигания. Шаг: " + (i + 1));

            Thread.sleep(1000 + (int) (Math.random() * 3000));
            System.out.println("Итерация " + (i + 1) + " завершилась");
        }
    }

    /**
     * Метод включает / отключает реле клемы аккамулятора
     */
    public void customPowerOnOffWithSleep(int numberOfAttempts, int sleepTime, LocalBaseUrl localBaseUrl) throws InterruptedException {
        for (int i = 0; i < numberOfAttempts; i++) {
            RestAssured.get(localBaseUrl.sendPowerOn);
            System.out.println("Сработала команда включения зажигания. Шаг: " + i);

            waitingBetweenAttempts(sleepTime);

            RestAssured.get(localBaseUrl.sendPowerOff);
            System.out.println("Сработала команда выключения зажигания. Шаг: " + i);

            Thread.sleep(1000 + (int) (Math.random() * 3000));
            System.out.println("Итерация " + i + " завершилась");
        }
    }

    /**
     * Кастомное ожидание между шагами
     *
     * @param sleepTime - время ожидания
     * @throws InterruptedException
     */
    public void waitingBetweenAttempts(int sleepTime) throws InterruptedException {
        System.out.println("Ждем ~ " + sleepTime / 1000 + " секунд");
        Thread.sleep(sleepTime);
    }

    /**
     * Включение зажигания
     */
    public Response accOn(LocalBaseUrl localBaseUrl) {
        return RestAssured.get(localBaseUrl.sendAccOn);
    }

    /**
     * Выключение зажигания
     */
    public Response accOff(LocalBaseUrl localBaseUrl) {
        return RestAssured.get(localBaseUrl.sendAccOff);
    }

    /**
     * Включение питания
     */
    public Response powerOn(LocalBaseUrl localBaseUrl) {
        return RestAssured.get(localBaseUrl.sendPowerOn);
    }

    /**
     * Выключение питания
     */
    public Response powerOff(LocalBaseUrl localBaseUrl) {
        return RestAssured.get(localBaseUrl.sendPowerOff);
    }

}
