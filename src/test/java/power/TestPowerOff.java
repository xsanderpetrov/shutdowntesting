package power;

import io.restassured.internal.RestAssuredResponseImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestPowerOff extends Base {

    @Test
    @DisplayName("Тест выключает питание")
    public void testPowerOff() {
        RestAssuredResponseImpl response = (RestAssuredResponseImpl) logic.powerOff(localBaseUrl);

        assertNotNull(response, "Пришел нулевой ответ");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, response);
    }
}
