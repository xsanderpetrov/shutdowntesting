package power;

import io.restassured.internal.RestAssuredResponseImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestPowerOn extends Base {

    @Test
    @DisplayName("Тест включает питание")
    public void testPowerOn() {
        RestAssuredResponseImpl response = (RestAssuredResponseImpl) logic.powerOn(localBaseUrl);

        assertNotNull(response, "Пришел нулевой ответ");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, response);
    }
}
