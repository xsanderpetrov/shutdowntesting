package power;

import io.restassured.internal.RestAssuredResponseImpl;
import logic.Logic;
import model.LocalBaseUrl;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Base {

    LocalBaseUrl localBaseUrl;
    Logic logic;
    int expectedStatusCode = 200;
    String expectedStatusLine = "HTTP/1.1 200 OK";
    String expectedContentType = "text/json";

    @BeforeEach
    public void setUp() {
        localBaseUrl = new LocalBaseUrl();
        logic = new Logic();
    }

    /**
     * Метод проверяет возвращаемые данные от api
     *
     * @param expectedStatusCode
     * @param expectedStatusLine
     * @param expectedContentType
     * @param response
     */
    public void checkStatus(int expectedStatusCode, String expectedStatusLine, String expectedContentType, RestAssuredResponseImpl response) {
        assertAll("Проверяем возвращаемые данные от API",
                () -> assertEquals(expectedStatusCode, response.getGroovyResponse().getStatusCode(), "Пришел не корректный статус код"),
                () -> assertEquals(expectedStatusLine, response.getGroovyResponse().getStatusLine(), "Пришел не верный статус лайн"),
                () -> assertEquals(expectedContentType, response.getGroovyResponse().getContentType(), "Пришел не верный тип данных")
        );
    }
}
