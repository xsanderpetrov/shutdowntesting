package power;

import io.restassured.internal.RestAssuredResponseImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestAccOnOffWithCustomTime extends Base {
    int timeNotLess = 600000;
    int timeNoMore = 0;
    int numberOfAttempts = 50;

    /**
     * Тест включает / отключает зажигание с заданным интервалом времени
     * 1. Выключаем зажигание и проверяем, что пришел корректный ответ api
     * 2. Проводим длительный тест на включение / отключение зажигания
     * 3. Возвращаем систему в исходное состояние
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Тест включает / отключает зажигание с заданным интервалом времени")
    public void testAccOnOffWithCustomTime() throws InterruptedException {

        RestAssuredResponseImpl responsePowerBefore = (RestAssuredResponseImpl) logic.powerOn(localBaseUrl);
        assertNotNull(responsePowerBefore, "Пришел нулевой ответ после включения зажигания");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, responsePowerBefore);
        System.out.println("Подаем питание на ГУ");

        RestAssuredResponseImpl responseAccBefore = (RestAssuredResponseImpl) logic.accOff(localBaseUrl);

        assertNotNull(responseAccBefore, "Пришел нулевой ответ после выключения ACC");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, responseAccBefore);

        int sleepTime = (timeNotLess + (int) (Math.random() * timeNoMore));
        logic.customAccOnOffWithSleep(numberOfAttempts, sleepTime, localBaseUrl);

        RestAssuredResponseImpl responseAfter = (RestAssuredResponseImpl) logic.accOff(localBaseUrl);
        assertNotNull(responseAfter, "Пришел нулевой ответ после выключения ACC");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, responseAfter);

        RestAssuredResponseImpl responsePowerOff = (RestAssuredResponseImpl) logic.powerOff(localBaseUrl);
        assertNotNull(responsePowerOff, "Пришел нулевой ответ после выключения питания");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, responsePowerOff);

        System.out.println("Выключаем питание ГУ");
    }
}
