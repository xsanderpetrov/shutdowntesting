package power;

import io.restassured.internal.RestAssuredResponseImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestPowerOnOffWithCustomTime extends Base {
    int timeNotLess = 600000;
    int timeNoMore = 0;
    int numberOfAttempts = 50;

    /**
     * Тест включает / отключает питание ГУ с заданным интервалом времени
     * 1. Выключаем питание ГУ и проверяем, что пришел корректный ответ api
     * 2. Проводим длительный тест на включение / отключение питания
     * 3. Возвращаем систему в исходное состояние
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Тест включает / отключает питание ГУ с заданным интервалом времени")
    public void testPowerOnOffWithCustomTime() throws InterruptedException {

        RestAssuredResponseImpl responsePowerBefore = (RestAssuredResponseImpl) logic.accOn(localBaseUrl);
        assertNotNull(responsePowerBefore, "Пришел нулевой ответ после включения зажигания");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, responsePowerBefore);
        System.out.println("Подаем питание на ACC");

        RestAssuredResponseImpl responseAccBefore = (RestAssuredResponseImpl) logic.powerOff(localBaseUrl);
        assertNotNull(responseAccBefore, "Пришел нулевой ответ после включения питания");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, responseAccBefore);

        int sleepTime = (timeNotLess + (int) (Math.random() * timeNoMore));
        logic.customPowerOnOffWithSleep(numberOfAttempts, sleepTime, localBaseUrl);

        RestAssuredResponseImpl responseAfter = (RestAssuredResponseImpl) logic.accOff(localBaseUrl);
        assertNotNull(responseAfter, "Пришел нулевой ответ после выключения зажигания");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, responseAfter);

        RestAssuredResponseImpl responsePowerOff = (RestAssuredResponseImpl) logic.powerOff(localBaseUrl);
        assertNotNull(responsePowerOff, "Пришел нулевой ответ после выключения питания");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, responsePowerOff);

        System.out.println("Выключаем питание ГУ");
    }
}
