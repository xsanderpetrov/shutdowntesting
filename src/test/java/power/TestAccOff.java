package power;

import io.restassured.internal.RestAssuredResponseImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestAccOff extends Base {

    @Test
    @DisplayName("Тест выключает зажигание")
    public void testAccOn() {
        RestAssuredResponseImpl response = (RestAssuredResponseImpl) logic.accOff(localBaseUrl);

        assertNotNull(response, "Пришел нулевой ответ");
        checkStatus(expectedStatusCode, expectedStatusLine, expectedContentType, response);
    }
}
